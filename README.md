# MathPlot: A Minetest Mod for Plotting Mathematical Objects

[MathPlot](https://content.minetest.net/packages/kyleclaassen/mathplot/) is a creative-mode [Minetest](https://www.minetest.net/) mod that provides tools for constructing mathematical curves/surfaces/solids. It is built with the intention of being used as an educational device in a multivariable&nbsp;/&nbsp;vector calculus course.

## Documentation

See the [project wiki](https://gitlab.com/kyleclaassen/minetest_mathplot/-/wikis/home) for setup instructions, documentation of the mod's features, and example usage.

Information about the author's use of Minetest in educational environments is available [here](https://www.rose-hulman.edu/~claassen/coolstuff/minetest_mathplot/).

## Contributors
* @powid &lt;powi@powi.fr&gt;
  + Implemented internationalization support and provided French translation.
