mathplot.gui.screens = mathplot.gui.screens or {}

local S = mathplot.get_translator
local E = minetest.formspec_escape

local function parse_and_validate(playername, identifier, fields, context)
    local e = {}  --list of error messages
    local p = table.copy(fields)

    if not mathplot.util.has_mathplot_priv(playername) then
        e[#e+1] = S("The 'mathplot' privilege is required.")
    end

    if minetest.string_to_pos(p.e1) == nil then
        e[#e+1] = S("Invalid +x Direction.")
    end
    if minetest.string_to_pos(p.e2) == nil then
        e[#e+1] = S("Invalid +y Direction.")
    end
    if minetest.string_to_pos(p.e3) == nil then
        e[#e+1] = S("Invalid +z Direction.")
    end

    --xmin, xmax, xstep validations
    local XMIN, ok = mathplot.evaluate_constant_expression(p.xmin)
    local xmin = tonumber(XMIN)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("x Min"), tostring(XMIN))
    elseif xmin == nil then
        e[#e+1] = S("x Min must be a number.")
    end
    local XMAX, ok = mathplot.evaluate_constant_expression(p.xmax)
    local xmax = tonumber(XMAX)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("x Max"), tostring(XMAX))
    elseif xmax == nil then
        e[#e+1] = S("x Max must be a number.")
    end
    if xmin ~= nil and xmax ~= nil and xmin > xmax then
        e[#e+1] = S("x Min must be <= x Max.")
    end
    local XSTEP, ok = mathplot.evaluate_constant_expression(p.xstep)
    local xstep = tonumber(XSTEP)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("x Step"), tostring(XSTEP))
    elseif xstep == nil then
        e[#e+1] = S("x Step must be a number.")
    end
    if xstep ~= nil and xstep <= 0 then
        e[#e+1] = S("x Step must be positive.")
    end

    --ymin, ymax, ystep validations
    local YMIN, ok = mathplot.evaluate_constant_expression(p.ymin)
    local ymin = tonumber(YMIN)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("y Min"), tostring(YMIN))
    elseif ymin == nil then
        e[#e+1] = S("y Min must be a number.")
    end
    local YMAX, ok = mathplot.evaluate_constant_expression(p.ymax)
    local ymax = tonumber(YMAX)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("y Max"), tostring(YMAX))
    elseif ymax == nil then
        e[#e+1] = S("y Max must be a number.")
    end
    if ymin ~= nil and ymax ~= nil and ymin > ymax then
        e[#e+1] = S("y Min must be <= y Max.")
    end
    local YSTEP, ok = mathplot.evaluate_constant_expression(p.ystep)
    local ystep = tonumber(YSTEP)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("y Step"), tostring(YSTEP))
    elseif ystep == nil then
        e[#e+1] = S("y Step must be a number.")
    end
    if ystep ~= nil and ystep <= 0 then
        e[#e+1] = S("y Step must be positive.")
    end

    --zmin, zmax, zstep validations
    local ZMIN, ok = mathplot.evaluate_constant_expression(p.zmin)
    local zmin = tonumber(ZMIN)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("z Min"), tostring(ZMIN))
    elseif zmin == nil then
        e[#e+1] = S("z Min must be a number.")
    end
    local ZMAX, ok = mathplot.evaluate_constant_expression(p.zmax)
    local zmax = tonumber(ZMAX)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("z Max"), tostring(ZMAX))
    elseif zmax == nil then
        e[#e+1] = S("z Max must be a number.")
    end
    if zmin ~= nil and zmax ~= nil and zmin > zmax then
        e[#e+1] = S("z Min must be <= z Max.")
    end
    local ZSTEP, ok = mathplot.evaluate_constant_expression(p.zstep)
    local zstep = tonumber(ZSTEP)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("z Step"), tostring(ZSTEP))
    elseif zstep == nil then
        e[#e+1] = S("z Step must be a number.")
    end
    if zstep ~= nil and zstep <= 0 then
        e[#e+1] = S("z Step must be positive.")
    end

    --Load ftn code string to check for syntax error
    local syntaxerror = mathplot.check_function_syntax(p.ftn, p.varnames, {0, 0, 0})
    if syntaxerror ~= nil then
        e[#e+1] = S("Syntax error in relation: @1", syntaxerror)
    end

    if not mathplot.util.is_drawable_node(fields.nodename) then
        e[#e+1] = S("'@1' is not a drawable node.", tostring(fields.nodename))
    end

    if #e == 0 then
        return p, e
    end
    --If errors, return original values
    return fields, e
end


local function load_saved_params(context)
    local meta = minetest.get_meta(context.node_pos)
    local s = meta:get_string("implicit_plot_params")
    return mathplot.util.merge_tables(
        mathplot.plotdefaults.plot_implicit_params(),
        minetest.deserialize(s) or {}
    )
end
local function have_saved_params(context, identifier)
    local meta = minetest.get_meta(context.node_pos)
    local s = meta:get_string("implicit_plot_params")
    return s ~= nil and string.trim(s) ~= ""
end

mathplot.gui.screens["implicit_plot"] = {
    initialize = function(playername, identifier, context)
        --If context.screen_params is already in context, then show those values.
        --(Likely coming back from a validation error.)
        if not context.screen_params then
            context.screen_params = load_saved_params(context)
        end
        return context
    end,
    get_formspec = function(playername, identifier, context)
        local p = context.screen_params
        local nodepos = context.node_pos

        mathplot.gui.set_brushes(playername, {brush = p.nodename})

        local allowErase = have_saved_params(context, identifier)

        local relationTooltipText = S("A function f(x,y,z), e.g. 'x^2 + y^2 + z^2 - 100'. A node will be set at (x,y,z) if f(x,y,z)=0. Can also use inequalities, e.g. 'x^2 + y^2 <= 100'.")

        local formspec = "size[12,10.5]"
        .. string.format("label[0,0;%s]", S("Implicit Plot"))
        .. "container[0,1]"
        .. string.format("label[0,0;%s]field[2,0;2,1;e1;;%s]", S("+x Direction:"), E(p.e1))
        .. string.format("label[4,0;%s]field[6,0;2,1;e2;;%s]", S("+y Direction:"), E(p.e2))
        .. string.format("label[8,0;%s]field[10,0;2,1;e3;;%s]", S("+z Direction:"), E(p.e3))
        .. string.format("label[0,1;%s]field[2,1;2,1;xmin;;%s]", S("x Min:"), E(p.xmin))
        .. string.format("label[4,1;%s]field[6,1;2,1;xmax;;%s]", S("x Max:"), E(p.xmax))
        .. string.format("label[8,1;%s]field[10,1;2,1;xstep;;%s]", S("x Step:"), E(p.xstep))
        .. string.format("label[0,2;%s]field[2,2;2,1;ymin;;%s]",  S("y Min:"), E(p.ymin))
        .. string.format("label[4,2;%s]field[6,2;2,1;ymax;;%s]", S("y Max:"), E(p.ymax))
        .. string.format("label[8,2;%s]field[10,2;2,1;ystep;;%s]", S("y Step:"), E(p.ystep))
        .. string.format("label[0,3;%s]field[2,3;2,1;zmin;;%s]", S("z Min:"), E(p.zmin))
        .. string.format("label[4,3;%s]field[6,3;2,1;zmax;;%s]", S("z Max:"), E(p.zmax))
        .. string.format("label[8,3;%s]field[10,3;2,1;zstep;;%s]", S("z Step:"), E(p.zstep))
        .. string.format("label[0,4;%s]field[2,4;10,1;ftn;;%s]", S("Relation:"), E(p.ftn))
        .. string.format("tooltip[ftn;%s]", E(relationTooltipText))
        .. "container_end[]"
        .. "container[0,6]"
        .. "list[current_player;main;0,0;8,4;]"
        .. string.format("label[8.25,0.25;%s]", S("Plot node:"))
        .. "list[detached:mathplot:inv_brush_" .. playername .. ";brush;9.75,0;1,1;]"
        .. "image[10.81,0.1;0.8,0.8;creative_trash_icon.png]"
        .. "list[detached:mathplot:inv_trash;main;10.75,0;1,1;]"
        .. "container_end[]"
        .. string.format("button_exit[0,10;2,1;btn_plot;%s]", S("Plot"))
        .. string.format("button_exit[2,10;2,1;btn_cancel;%s]", S("Cancel"))
        .. (allowErase and string.format("button_exit[9,10;3,1;btn_erase;%s]", S("Erase Previous")) or "")
        return formspec
    end,
    on_receive_fields = function(playername, identifier, fields, context)
        if fields.btn_plot or fields.key_enter or fields.btn_erase then
            local nodename = ""
            local newfields = nil
            if fields.btn_erase then
                newfields = load_saved_params(context)
                newfields.nodename = "air"
                context.is_erase = true
            else
                nodename = mathplot.gui.get_brushes(playername, { "brush" })["brush"]
                newfields = mathplot.util.merge_tables(
                    mathplot.plotdefaults.plot_implicit_params(),
                    fields,
                    { origin_pos = context.node_pos, nodename = nodename }
                )
            end

            mathplot.gui.validate_screen_form(playername, identifier, newfields, context,
                {
                    validator_function = parse_and_validate,
                    success_callback = function(playername, identifier, validated_params, context)
                        if not context.is_erase then
                            local nodemeta = minetest.get_meta(validated_params.origin_pos)
                            nodemeta:set_string("implicit_plot_params", minetest.serialize(validated_params))
                        end

                        return mathplot.plot_implicit(validated_params, playername)
                    end,
                    -- failure_callback = function(errormsgs, playername, identifier, fields, context)
                    --     return true, errormsgs
                    -- end
                })
        end
    end
}
