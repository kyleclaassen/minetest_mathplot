mathplot.gui.screens = mathplot.gui.screens or {}

local S = mathplot.get_translator

mathplot.gui.screens["delete_node"] = {
    initialize = function(playername, identifier, context)
    end,
    get_formspec = function(playername, identifier, context)
        local prompt = S("Really delete this origin node? All equations will be lost.")
        local formspec = "size[8.5,2]"
        .. string.format("textarea[0.25,0;8.5,1.25;;;%s]", prompt)
        .. string.format("button_exit[0,1.5;3,1;btn_OK;%s]", minetest.colorize("#ff7777", S("Delete Node")))
        .. string.format("button_exit[4,1.5;3,1;btn_cancel;%s]", S("Cancel"))
        return formspec
    end,
    on_receive_fields = function(playername, identifier, fields, context)
        if fields.btn_OK then
            --Note: calling minetest.node_dig() invokes node methods, e.g. after_destruct(),
            --      so calling it will automatically handle protection and
            --      removing the node from mod storage.
            local digger = minetest.get_player_by_name(playername)
            local node = minetest.get_node(context.node_pos)
            minetest.node_dig(context.node_pos, node, digger)
        end
    end
}
