mathplot.gui.screens = mathplot.gui.screens or {}

local S = mathplot.get_translator
local E = minetest.formspec_escape

local function parse_and_validate(playername, identifier, fields, context)
    local e = {}  --list of error messages
    local p = table.copy(fields)

    if not mathplot.util.has_mathplot_priv(playername) then
        e[#e+1] = S("The 'mathplot' privilege is required.")
    end

    if minetest.string_to_pos(p.e1) == nil then
        e[#e+1] = S("Invalid +x Direction.")
    end
    if minetest.string_to_pos(p.e2) == nil then
        e[#e+1] = S("Invalid +y Direction.")
    end
    if minetest.string_to_pos(p.e3) == nil then
        e[#e+1] = S("Invalid +z Direction.")
    end

    --tmin, tmax, tstep validations
    local TMIN, ok = mathplot.evaluate_constant_expression(p.tmin)
    local tmin = tonumber(TMIN)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("t Min"), tostring(TMIN))
    elseif tmin == nil then
        e[#e+1] = S("t Min must be a number.")
    end
    local TMAX, ok = mathplot.evaluate_constant_expression(p.tmax)
    local tmax = tonumber(TMAX)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("t Max"), tostring(TMAX))
    elseif tmax == nil then
        e[#e+1] = S("t Max must be a number.")
    end
    local TSTEP, ok = mathplot.evaluate_constant_expression(p.tstep)
    local tstep = tonumber(TSTEP)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", S("t Step"), tostring(TSTEP))
    elseif tstep == nil then
        e[#e+1] = S("t Step must be a number.")
    end
    if tstep ~= nil and tstep <= 0 then
        e[#e+1] = S("t Step must be positive.")
    end

    local T0, ok = mathplot.evaluate_constant_expression(p.t0)
    local t0 = tonumber(T0)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", "t0", tostring(T0))
    elseif t0 == nil then
        e[#e+1] = S("t0 must be a number.")
    end

    if tmin ~= nil and tmax ~= nil then
        if tmin > tmax then
            e[#e+1] = S("t Min must be <= t Max.")
        elseif t0 ~= nil and (t0 < tmin or t0 > tmax) then
            e[#e+1] = S("Must have t Min <= t0 <= t Max.")
        end
    end

    local X0, ok = mathplot.evaluate_constant_expression(p.x0)
    local x0 = tonumber(X0)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", "x(t0)", tostring(X0))
    elseif x0 == nil then
        e[#e+1] = S("x(t0) must be a number.")
    end
    --
    local Y0, ok = mathplot.evaluate_constant_expression(p.y0)
    local y0 = tonumber(Y0)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", "y(t0)", tostring(Y0))
    elseif y0 == nil then
        e[#e+1] = S("y(t0) must be a number.")
    end
    --
    local Z0, ok = mathplot.evaluate_constant_expression(p.z0)
    local z0 = tonumber(Z0)
    if not ok then
        e[#e+1] = S("Unable to determine @1: @2", "z(t0)", tostring(Z0))
    elseif z0 == nil then
        e[#e+1] = S("z(t0) must be a number.")
    end

    --Load ftn code string to check for syntax error
    local syntaxerror = mathplot.check_function_syntax(p.ftn_x_prime, p.varnames, {0, 0, 0, 0})
    if syntaxerror ~= nil then
        e[#e+1] = S("Syntax error in @1: @2", "dx/dt", syntaxerror)
    end
    syntaxerror = mathplot.check_function_syntax(p.ftn_y_prime, p.varnames, {0, 0, 0, 0})
    if syntaxerror ~= nil then
        e[#e+1] = S("Syntax error in @1: @2", "dy/dt", syntaxerror)
    end
    syntaxerror = mathplot.check_function_syntax(p.ftn_z_prime, p.varnames, {0, 0, 0, 0})
    if syntaxerror ~= nil then
        e[#e+1] = S("Syntax error in @1: @2", "dz/dt", syntaxerror)
    end

    if not mathplot.util.is_drawable_node(fields.nodename) then
        e[#e+1] = S("'@1' is not a drawable node.", tostring(fields.nodename))
    end

    p.ode_method = string.lower(string.trim(p.ode_method))
    if mathplot.get_ODE_methods()[p.ode_method] == nil then
        e[#e+1] = S("Invalid ODE method: @1", fields.ode_method)
    end

    if #e == 0 then
        return p, e
    end
    --If errors, return original values
    return fields, e
end


local function load_saved_params(context)
    local meta = minetest.get_meta(context.node_pos)
    local s = meta:get_string("ODE_plot_params")
    return mathplot.util.merge_tables(
        mathplot.plotdefaults.plot_ODE_params(),
        minetest.deserialize(s) or {}
    )
end
local function have_saved_params(context, identifier)
    local meta = minetest.get_meta(context.node_pos)
    local s = meta:get_string("ODE_plot_params")
    return s ~= nil and string.trim(s) ~= ""
end

mathplot.gui.screens["ODE_plot"] = {
    initialize = function(playername, identifier, context)
        --If context.screen_params is already in context, then show those values.
        --(Likely coming back from a validation error.)
        if not context.screen_params then
            context.screen_params = load_saved_params(context)
        end
        return context
    end,
    get_formspec = function(playername, identifier, context)
        local p = context.screen_params
        local nodepos = context.node_pos

        mathplot.gui.set_brushes(playername, {brush = p.nodename})

        local allowErase = have_saved_params(context, identifier)
        local formspec = "size[12,11.5]"
        .. string.format("label[0,0;%s]", S("ODE Plot"))
        .. "container[0,1]"
        .. string.format("label[0,0;%s]field[2,0;2,1;e1;;%s]", S("+x Direction:"), E(p.e1))
        .. string.format("label[4,0;%s]field[6,0;2,1;e2;;%s]", S("+y Direction:"), E(p.e2))
        .. string.format("label[8,0;%s]field[10,0;2,1;e3;;%s]", S("+z Direction:"), E(p.e3))
        .. string.format("label[0,1;%s]field[2,1;2,1;tmin;;%s]", S("t Min:"), E(p.tmin))
        .. string.format("label[4,1;%s]field[6,1;2,1;tmax;;%s]", S("t Max:"), E(p.tmax))
        .. string.format("label[8,1;%s]field[10,1;2,1;tstep;;%s]", S("t Step:"), E(p.tstep))
        .. string.format("label[0,2;%s]field[2,2;2,1;t0;;%s]", E("t0 ="), E(p.t0))
        .. string.format("label[0,3;%s]field[2,3;2,1;x0;;%s]", E("x(t0) ="), E(p.x0))
        .. string.format("label[0,4;%s]field[2,4;2,1;y0;;%s]", E("y(t0) ="), E(p.y0))
        .. string.format("label[0,5;%s]field[2,5;2,1;z0;;%s]", E("z(t0) ="), E(p.z0))
        .. string.format("label[4,3;%s]field[6,3;6,1;ftn_x_prime;;%s]", "dx / dt =", E(p.ftn_x_prime))
        .. string.format("label[4,4;%s]field[6,4;6,1;ftn_y_prime;;%s]", "dy / dt =", E(p.ftn_y_prime))
        .. string.format("label[4,5;%s]field[6,5;6,1;ftn_z_prime;;%s]", "dz / dt =", E(p.ftn_z_prime))
        .. "container_end[]"
        .. "container[0,7]"
        .. "list[current_player;main;0,0;8,4;]"
        .. string.format("label[8.25,0.25;%s]", S("Plot node:"))
        .. "list[detached:mathplot:inv_brush_" .. playername .. ";brush;9.75,0;1,1;]"
        .. "image[10.81,0.1;0.8,0.8;creative_trash_icon.png]"
        .. "list[detached:mathplot:inv_trash;main;10.75,0;1,1;]"
        .. string.format("label[8.25,1.3;%s]field[10,1.35;2,1;ode_method;;%s]", S("Method:"), E(p.ode_method))
        .. string.format("tooltip[ode_method;%s (%s)]", S("Method used to solve the ODE system."), E(mathplot.util.ODE_methods_string()))
        .. string.format("checkbox[8.25,2;chk_connect;%s;%s]", S("Connected"), E(tostring(p.connect)))
        .. string.format("tooltip[chk_connect;%s]", S("Connect the curve points with line segments."))
        .. "container_end[]"
        .. string.format("button_exit[0,11;2,1;btn_plot;%s]", S("Plot"))
        .. string.format("button_exit[2,11;2,1;btn_cancel;%s]", S("Cancel"))
        .. (allowErase and string.format("button_exit[9,11;3,1;btn_erase;%s]", S("Erase Previous")) or "")
        return formspec
    end,
    on_receive_fields = function(playername, identifier, fields, context)
        if fields.chk_connect ~= nil then
            --Checkbox field chk_connect is not sent on button press, so store it on the context
            context.screen_params.connect = minetest.is_yes(fields.chk_connect)
        end

        if fields.btn_plot or fields.key_enter or fields.btn_erase then
            local nodename = ""
            local newfields = nil
            if fields.btn_erase then
                newfields = load_saved_params(context)
                newfields.nodename = "air"
                context.is_erase = true
            else
                nodename = mathplot.gui.get_brushes(playername, { "brush" })["brush"]
                newfields = mathplot.util.merge_tables(
                    mathplot.plotdefaults.plot_ODE_params(),
                    fields,
                    { origin_pos = context.node_pos, nodename = nodename, connect = context.screen_params.connect }
                )
            end

            mathplot.gui.validate_screen_form(playername, identifier, newfields, context,
                {
                    validator_function = parse_and_validate,
                    success_callback = function(playername, identifier, validated_params, context)
                        if not context.is_erase then
                            local nodemeta = minetest.get_meta(validated_params.origin_pos)
                            nodemeta:set_string("ODE_plot_params", minetest.serialize(validated_params))
                        end

                        return mathplot.plot_ODE(validated_params, playername)
                    end,
                    -- failure_callback = function(errormsgs, playername, identifier, fields, context)
                    --     return true, errormsgs
                    -- end
                })
        end
    end
}
